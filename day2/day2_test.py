import unittest
from day2 import day2


class TestDay2(unittest.TestCase):
    def test_sortChars(self):
        self.assertEqual(day2.sortChars('abcdab'), 'aabbcd')

    def test_hasDuplicateCharsTrue(self):
        self.assertTrue(day2.hasDuplicateChars('aaabbcd'))

    def test_hasDuplicateCharsFalse(self):
        self.assertFalse(day2.hasDuplicateChars('aaabcd'))

    def test_hasTriplicateCharsTrue(self):
        self.assertTrue(day2.hasTriplicateChars('aaabcd'))

    def test_hasTriplicateCharsFalse(self):
        self.assertFalse(day2.hasTriplicateChars('aabcd'))

    def test_getDuplicateAmount(self):
        self.assertEqual(day2.getDuplicateAmount(
            ['aabcd', 'abbcd', 'aaacde']), 2)

    def test_getTriplicateAmount(self):
        self.assertEqual(day2.getTriplicateAmount(
            ['aabcd', 'abbcd', 'aaacde']), 1)

    def test_getDifference(self):
        self.assertEqual(day2.getDifference('baaaaa', 'aaaaac'), 2)


if __name__ == '__main__':
    unittest.main()
