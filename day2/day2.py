import re
import os


def sortChars(str):
    return ''.join(sorted(str))


def _getBefore(index, arr):
    if index > 0:
        return arr[index - 1]
    return arr[index]+'x'


def _getAfter(index, arr):
    if index < len(arr) - 1:
        return arr[index + 1]
    return arr[index - 1]+'x'


def hasDuplicateChars(str):
    for index, char in enumerate(str):
        before = _getBefore(index, str)
        after = _getAfter(index, str)
        afterplus = _getAfter(index + 1, str)
        if char != before and char != afterplus and char == after:
            return True
    pass
    return False


def hasTriplicateChars(str):
    rgx = re.compile(r'(\w)\1{2}')
    matches = rgx.search(str)
    return bool(matches)


def getDuplicateAmount(arr):
    result = 0
    for str in arr:
        if hasDuplicateChars(str):
            result += 1

    return result


def getTriplicateAmount(arr):
    result = 0
    for str in arr:
        if hasTriplicateChars(str):
            result += 1

    return result


def getData():
    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    file = os.path.join(THIS_FOLDER, 'day2_data.txt')

    text_file = open(file, "r")
    lines = text_file.readlines()
    text_file.close()
    return lines


def getDifference(str1, str2):
    if str1 == str:
        return -1

    count = 0
    for index, char in enumerate(str1):
        if char != str2[index]:
            count += 1

    return count


def getDifferencePosition(str1, str2):
    for index, char in enumerate(str1):
        if char != str2[index]:
            return index


def getSimilar(arr):
    for str1 in arr:
        arr2 = arr.copy()
        arr2.remove(str1)

        for str2 in arr2:
            diff = getDifference(str1, str2)
            if diff == 2:
                print(str1, str2)
            if diff == 1:
                return [str1, str2]


def difference():
    data = getData()
    similar = getSimilar(data)
    pos = getDifferencePosition(similar[0], similar[1])
    cleaned = similar[0][:pos] + similar[0][pos + 1:]

    print(similar[0])
    print(similar[1])
    print(cleaned)


def checksum():
    data = list(map(sortChars, getData()))
    dups = getDuplicateAmount(data)
    trips = getTriplicateAmount(data)

    print(dups * trips)


if __name__ == "__main__":
    difference()
    pass
