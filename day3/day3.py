import os
import numpy as np


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Fabric:
    def __init__(self, id, left, top, width, height):
        self.id = id
        self.left = left
        self.top = top
        self.width = width
        self.height = height

    def topLeft(self):
        return Point(self.left, self.top)

    def topRight(self):
        return Point(self.left + self.width, self.top)

    def bottomLeft(self):
        return Point(self.left, self.top + self.height)

    def bottomRight(self):
        return Point(self.left + self.width, self.top + self.height)


def toInt(str):
    return int(float(str))


def toFabric(input: str):
    args = list(map(toInt, input.rstrip().split(',')))
    return Fabric(args[0], args[1], args[2], args[3], args[4])


def getData():
    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    file = os.path.join(THIS_FOLDER, 'day3_data.txt')

    text_file = open(file, "r")
    lines = text_file.readlines()
    text_file.close()

    return list(map(toFabric, lines))


def getChar(i):
    if i == 0:
        return '.'
    elif i == 1:
        return '░'
    elif i == 2:
        return '▒'
    elif i == 3:
        return '▓'
    elif i > 3:
        return '█'


def getOverlap(data):
    fabric = np.zeros((1000, 1000), int)

    for entry in data:
        for y in range(entry.topLeft().y, entry.bottomLeft().y):
            for x in range(entry.topLeft().x, entry.topRight().x):
                fabric[y][x] += 1

    count = 0
    for e in fabric:
        # print(''.join(list(map(getChar, e))))
        # print(e)
        for p in e:
            if p > 1:
                count += 1
    print(count)


if __name__ == "__main__":
    getOverlap(getData())
    pass
