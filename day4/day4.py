import os
import numpy as np


def splitStr(s: str):
    return s.rstrip().split(',')


def isId(s):
    return s[4] != 'up' and s[4] != 'asleep'


def fallenAsleep(s):
    return s[4] == 'asleep'


def wokeUp(s):
    return s[4] == 'awake'


def getInt(s):
    return int(float(s))


def builDict():
    data = getData()

    guards = dict()
    splitEntries = list(map(splitStr, data))
    asleepStart = 0
    asleepEnd = 0
    currentGuard = ''

    for entry in splitEntries:
        if isId(entry):
            currentGuard = entry[4]
            asleepStart = 0
            asleepEnd = 0
            if currentGuard not in guards:
                guards[currentGuard] = np.zeros(60, int)
        else:
            if fallenAsleep(entry):
                asleepStart = getInt(entry[3])
            else:
                asleepEnd = getInt(entry[3])
                for index in range(asleepStart, asleepEnd):
                    guards[currentGuard][index] += 1

    return guards


def getLongestSleeper():
    guards = builDict()
    longest = '', 0

    for guard in guards.keys():
        s = sum(guards[guard], 0)
        if s > longest[1]:
            longest = guard, s

    highest = 0, 0
    for index, num in enumerate(guards[longest[0]]):
        if num > highest[1]:
            highest = index, num

    print(longest[0], highest[0])


def getHighestFreq():
    guards = builDict()

    guardMinuteAmount = '', 0, 0

    for index in range(0, 60):
        for g in guards:
            if guards[g][index] > guardMinuteAmount[2]:
                guardMinuteAmount = g, index, guards[g][index]

    print(guardMinuteAmount)


def getData():
    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    file = os.path.join(THIS_FOLDER, 'day4_data.txt')

    text_file = open(file, "r")
    lines = text_file.readlines()
    text_file.close()

    return lines


if __name__ == "__main__":
    getHighestFreq()
    # getLongestSleeper()
