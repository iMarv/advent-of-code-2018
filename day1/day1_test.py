import unittest
from day1 import day1


class TestDay1(unittest.TestCase):
    def test_getFrequency(self):
        self.assertEqual(day1.getFrequency([+1, -2, +3, +1]), 3)

    def test_firstDuplicateFrequency1(self):
        self.assertEqual(day1.getFirstDuplicateFrequency(
            [+1, -2, +3, +1]), 2)

    def test_firstDuplicateFrequency2(self):
        self.assertEqual(day1.getFirstDuplicateFrequency(
            [+3, +3, +4, -2, -4]), 10)

    def test_firstDuplicateFrequency3(self):
        self.assertEqual(day1.getFirstDuplicateFrequency(
            [-6, +3, +8, +5, -6]), 5)

    def test_firstDuplicateFrequency4(self):
        self.assertEqual(day1.getFirstDuplicateFrequency(
            [+7, +7, -2, -7, -4]), 14)


if __name__ == '__main__':
    unittest.main()
