import os


def getFrequency(freqs):
    result = 0
    for freq in freqs:
        result += freq
        pass
    return result


def getFirstDuplicateFrequency(inputs: list):
    frequency = 0
    duplicates = set([])

    while 1 == 1:
        for freq in inputs:
            frequency += freq

            if frequency in duplicates:
                return frequency
            else:
                duplicates.add(frequency)
            pass
        pass


def getNum(str):
    return int(float(str))


def getData():
    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    file = os.path.join(THIS_FOLDER, 'day1_data.txt')

    text_file = open(file, "r")
    lines = text_file.readlines()
    text_file.close()
    return list(map(getNum, lines))


def main():
    # print(getFrequency(getData()))
    dup = getFirstDuplicateFrequency(getData())
    print(dup)


if __name__ == "__main__":
    main()
